# Budget

iOS/iPadOS aplikácia

## Definícia problému

Zákazník požaduje aplikáciu pre sledovanie budgetu, ktorú môže využívať na viacerých zariadeniach. Zákazník požaduje možnosť sledovať viacej budgetov (súkromný, rodinný, ...). Všetky výdaje a výnosy sa ukladajú lokálne pomocou CoreData. Využite CloudKit pre synchronizáciu dát medzi zariadeniami.

## Špecifikácia požiadaviek

- Aplikácia umožňuje spravovať viacej budgetov
- Dáta sú ukladané na iCloud a automaticky synchronizované medzi viacerými zariadeniami
- Do aplikácie je možné pridávať výdaje aj výnosy spolu s poznámkou
- V aplikácii je možné zobraziť výdaje a výnosy pre konkrétny mesiac

## Možné rozšírenia

- Okrem poznámky je možné pridať aj fotografiu 

## Zdroje

- [Zrkadlenie CoreData pomocou CloudKitu](https://developer.apple.com/documentation/coredata/mirroring_a_core_data_store_with_cloudkit)