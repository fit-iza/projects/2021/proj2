# DB Logs

Knižnica pre iOS, iPadOS alebo MacOS aplikáciu využívajúcu CoreData

## Definícia problému

Zákazník vlastniaci viacej aplikácií vyžaduje ukladanie všetkých zmien v databáze vo forme textových logov. Požaduje od Vás vytvorenie knižnice, ktorú by mohol integrovať do svojej aplikácie, ktorá sleduje všetky zmeny v databáze CoreData a ukladá ich ako logy.

## Špecifikácia požiadaviek

- Knižnica bude vytvorená pomocou Swift Package Manager
- Knižnica bude využívať Combine
- Logy budú vypisované na STDOUT (nie je potrebné ich nikam ukladať) 
- Logy sa vypíšu raz za 5 sekúnd (využite funkciu throttle z knižnice Combine)
- Predpokladajte, že každý objekt uložený v CoreData má property `id` typu `UUID`, ktorá slúži ako unikátny identifikátor
- Každý log obsahuje časovú známku, identifikátor objektu (`id`), meno zmeneného atribútu, starú a novú hodnotu
- Funkčnosť knižnice prezentujte pomocou Vami vytvorenej primitívnej aplikácie (napríklad pre správu poznámok)

## Zdroje

- [sledovanie zmien v CoreData](https://www.donnywals.com/responding-to-changes-in-a-managed-object-context/)
- [Combine](https://heckj.github.io/swiftui-notes/)