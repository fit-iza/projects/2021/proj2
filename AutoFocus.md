# AutoFocus

Aplikácia pre iOS alebo iPadOS

## Definícia problému

Potrebujete vytvoriť aplikáciu typu kamera, ktorá automaticky zaostruje na tvár v obraze.

## Špecifikácia požiadaviek

- Aplikácia využíva zadnú kameru
- Zaostrovací bod (focusPointOfInterest) sa automaticky mení podľa pozície tváre v obraze
- V UI vhodne prezentujte aktuálnu hodnotu zaostrovacieho bodu
- Pre detekciu tváre využite už existujúcu knižnicu (napr. Vision od Apple)
- Aplikácia nemusí vedieť ukladať fotky alebo video

## Možné rozšírenia

- Používateľ má možnosť vybrať aj inú zadnú kameru (vhodné pre zariadenia s viacerými zadnými kamerami)
- Aplikácia umožňuje vytvárať a ukladať fotografie
- Aplikácia umožňuje vytvárať a ukladať video

## Zdroje:

- [Rozpoznávanie tváre](https://developer.apple.com/documentation/vision/tracking_the_user_s_face_in_real_time)
- [Zaostrenie na konkrétny bod](https://developer.apple.com/documentation/avfoundation/avcapturedevice/1385853-focuspointofinterest)