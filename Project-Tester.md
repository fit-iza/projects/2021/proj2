# Project tester

Webová aplikácia (Linux/MacOS)

## Definícia problému

Študenti by chceli mať možnosť testovať svoje projekty pomocou webovej stránky. Vytvorte webovú aplikáciu, do ktorej sa bude môcť študent prihlásiť a nahrať zip s projektom. Na serveri sa následne spustí skript s odovzdaným súborom a výpis s návratovým kódom sa zobrazí používateľovi. Odovzdaný zip aj s výsledkom testov sa uloží na server (výsledok testovania sa uloží do databázy).

## Špecifikácia požiadaviek

- Webová aplikácia bude napísaná v jazyku Swift a bude využívať knižnicu [Vapor](https://vapor.codes)
- Aplikácia bude využívať PostgreSQL
- Aplikácia využije websockety pre zobrazovanie aktuálneho stavu (priebeh testovania, výsledok testu, ...)
- UI nemusí byť nič extra, stačí základne CSS

## Možné rozšírenia

- Správa používateľov
- Adminská stránka
- Stránka pre vyučujúcich s prehľadom odovzdaných projektov

## Zdroje

- [Vapor](https://vapor.codes)