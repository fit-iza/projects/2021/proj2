# Public IP Checker

Konzolová aplikácia (MacOS/Linux)

## Definícia problému

Je potrebné detekovať zmenu verejnej IP adresy zariadenia, na ktorom sa pravidelne spúšťa Vami vytvorená aplikácia. Aplikácia odošle mail v prípade, kedy sa zmení verejná IP adresa zariadenia. Mailové adresy sú načítané z CSV súboru, mail obsahuje informáciu o čase kedy bola zmena detekovaná, pôvodnú IP adresu a aktuálnu IP adresu.

## Špecifikácia požiadaviek

- Aplikácia berie iba jeden argument s cestou k CSV súboru obsahujúceho emailové adresy
- Údaje k SMTP sa načítajú z environmentálnych premenných
- Aplikácia podporuje argument `-h`, `--help` a `help`, pri ktorých vypíše nápovedu k používaniu aplikácie vrátane kľúčov k používaným environmentálnym premenným a následne sa úspešne ukončí
- Detekovaná verejná IP adresa sa lokálne uloží tak, aby aplikácia fungovala aj na Linuxe
- Pre komunikáciu s SMTP použite knižnicu (napríklad [smtp-swift](https://github.com/sersoft-gmbh/swift-smtp))

## Možné rozšírenia

- Aplikácia podporuje viacej argumentov (napr.: hodnota pre timeout, cesta k šablóne emailu, ...)
- Výsledný kód aplikácie je možné použiť ako knižnicu pomocou Swift Package Managera

## Zdroje

- [smtp-swift](https://github.com/sersoft-gmbh/swift-smtp)
