# Projekt 2021

V tomto repozitári je uvedených niekoľko variant projektov do predmetu IZA na [VUT FIT v Brně](https://www.fit.vutbr.cz)

## Varianty

- [AutoFocus](AutoFocus.md) - iOS/iPadOS aplikácia pre automatické zaostrovanie na tvár **(1/6)**
- [Budget](Budget.md) - iOS/iPadOS aplikácia pre sledovanie budgetu **(2/6)**
- [DB Logs](DB-Logs.md) - Knižnica pre sledovanie zmien v databázovom systéme CoreData **(0/6)**
- [Drink Water](Drink-Water.md) - iOS(iPhone)+watchOS aplikácia na pripomínanie pitného režimu **(2/6)**
- [Project Tester](Project-Tester.md) - Webová aplikácia pre testovanie študentských projektov **(2/6)**
- [Public IP Checker](Public-IP-Checker.md) - Konzolová aplikácia pre sledovanie zmeny verejnej IP adresy **(6/6)**
- [Vlastné Zadanie](Custom.md) - Vlastné zadanie **(2/99)**


## Registrovanie variantov
Registrácia variant prebieha pomocou emailu kde ako adresát bude uvedený mail filip@klembara.pro a do kópie vložte hrubym@fit.vutbr.cz. Dodržte formát predmetu správy **IZA project registration** a v správe uveďte o aký projekt máte záujem. Maximálny počet študentov na jeden projekt je **3**. Zmenu variantu alebo jeho zrušenie riešte mailom s filip@klembara.pro. 

**Zaregistrovaním nejakej z týchto variant dávate súhlas so sprístupnením Vášho riešenia mne ako osobe bez pracovného pomeru s VUT FIT.**

## Licencia
Vaše riešenie musí byť pod licenciou [MIT](https://choosealicense.com/licenses/mit/) alebo [apache 2](https://choosealicense.com/licenses/apache-2.0/). Súbor *LICENSE* s obsahom Vami zvolenej licencie odovzdajte spolu s projektom. Súbor by mal byť v rovnakom priečinku ako je *.xcodeproj*.

## Odovzdanie
Deadline projektov je určený vo wise. Tam odovzdáte aj svoje riešenia. Odovzdajte všetky potrebné súbory *(LICENSE, PROJECT\_NAME.xcodeproj, PROJECT\_NAME.xcworkspace, PROJECT\_NAME/, ...)*.

## Hodnotenie
Finálne hodnotenie je výhradne v rukách garanta predmetu. Ja môžem iba odporučiť body. 

## Spoločné požiadavky

### Dokumentácia
Vypracujte jednoduchú dokumentáciu, v ktorej jednoducho popíšete ako výsledná aplikácia funguje, čo ste robili, aké ste mali problémy ich riešenia a čo nefunguje (priznanie nefungujúceho kódu je lepšie hodnotené ako odhalenie). Dokumentácie odovzdajte vo formáte *.pdf*. Nerozpisujte triviality a zbytočnosti, iOS je o jednoduchosti, tak to dodržte aj v dokumentácii.

### Dodržanie čistého kódu
- Použite nástroj [swiftlint](https://github.com/realm/SwiftLint)
- Dodržujte jednoduchosť aplikácie
- Nepíšte "1000" riadkové kontroléry
- Reálne používajte nejakú architektúru (MVC, MVVM, VIPER, ...) tak aby to bolo jasné už na prvý pohľad na kód
- Píšte konzistentné aplikácie (snažte sa neporušiť [HIG](https://developer.apple.com/design/human-interface-guidelines/))

### Knižnice tretích strán

Sú povolené knižnice tretích strán pokiaľ sa inštalujú pomocou [Swift Package Manager](https://swift.org/package-manager/)

